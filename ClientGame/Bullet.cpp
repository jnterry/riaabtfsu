#include "stdafx.h"
#include "Bullet.hpp"

Bullet::Bullet(float lifetime, Vector2 pos, Vector2 vel, int team) : mRemainingLifetime(lifetime), mTeam(team){
	this->SetPosition(pos);
	this->SetColor(Color(1, 1, 0));
	this->SetFriction(0);
	this->SetSize(Vector2(1, 0.5));
	this->SetDensity(3);
	this->InitPhysics();
	this->GetBody()->SetLinearVelocity(b2Vec2(vel.X, vel.Y));
	this->GetBody()->SetGravityScale(0);
}

void Bullet::Update(float dt){
	this->mRemainingLifetime -= dt;
	if (mRemainingLifetime <= 0){
		this->Destroy();
	}
}