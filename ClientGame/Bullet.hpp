#ifndef BULLET_HPP
#define BULLET_HPP

#include "stdafx.h"

class Bullet : public PhysicsActor{
public:
	Bullet(float lifetime, Vector2 pos, Vector2 vel, int team);

	void Update(float dt) override;
private:
	float mRemainingLifetime;
	int mTeam;
};

#endif