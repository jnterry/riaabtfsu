#include "stdafx.h"
#include "Enemy.hpp"
#include "WeaponLaser.hpp"

Enemy::Enemy(Vector2 pos) : HealthyEntity(50, 1, 1), mWeapon(new WeaponLaser(3)){
	Vector2 size = (2, 2);
	this->SetSize(size);
	this->SetPosition(pos + size);
	this->SetColor(Color(1, 0, 0));
	this->SetDensity(0.3);
	this->InitPhysics();
}


void Enemy::Update(float dt){
	mWeapon->Update(dt);
	mWeapon->fire(this->GetPosition());
}

