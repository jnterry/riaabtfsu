#ifndef ENEMY_HPP
#define ENEMY_HPP

#include "stdafx.h"
#include "HealthyEntity.hpp"
#include "Weapon.hpp"

class Enemy : public HealthyEntity{
public:
	Enemy(Vector2 pos);

	void Update(float dt) override;
private:
	Weapon* mWeapon;
};

#endif