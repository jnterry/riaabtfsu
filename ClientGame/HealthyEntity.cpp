#include "stdafx.h"
#include "HealthyEntity.hpp"

HealthyEntity::HealthyEntity(float maxHealth, float regenPerSecond, int team) 
	: mMaxHealth(maxHealth), mHealth(maxHealth), mRegen(regenPerSecond), mTeam(team){

}

void HealthyEntity::Update(float dt){
	if (mHealth <= 0){
		this->Destroy();
	} else {
		mHealth += mRegen*dt;
		if (mHealth > mMaxHealth){
			mHealth = mMaxHealth;
		}
	}
}