#ifndef HEALTHYENTITY_HPP
#define HEALTHYENTITY_HPP

#include "stdafx.h"

class HealthyEntity : public PhysicsActor{
public:
	HealthyEntity(float maxHealth, float regenPerSecond, int team);

	virtual void Update(float dt) override;
private:
	float mMaxHealth;
	float mHealth;
	float mRegen;
	int mTeam;
};

#endif