#include "stdafx.h"
#include "LudumGameManager.hpp"
#include "Player.hpp"
#include "Resources.hpp"

LudumGameManager::LudumGameManager() 
	: mPlaying(false), mPreviousScore(0), mBestScore(0), mMapManager(nullptr), mPlayer(nullptr){

	TextActor* scPressSpace = new TextActor("Console", "Press Space To Start", TXT_Center);
	scPressSpace->SetPosition(Vector2(0, 0));
	this->mStartScreenActors.push_back(scPressSpace);

	TextActor* scControls = new TextActor("Console", "Space to jump, A to shoot", TXT_Center);
	scControls->SetPosition(Vector2(0, -5));
	this->mStartScreenActors.push_back(scControls);

	showStartScreen();
}

void LudumGameManager::Start(){
	
}

void LudumGameManager::Stop(){
	stopGame();
}

void LudumGameManager::Update(float dt){
	if (mPlaying){
		this->mMapManager->Update(this->mPlayer->GetPosition());
		if (this->mPlayer->GetPosition().Y < 0 && !theSound.IsPlaying(res::snd::gameOver)){
			stopGame();
		}
	} else {
		if (theInput.IsKeyDown(GLFW_KEY_SPACE)){
			startGame();
		}
	}
	
}


void LudumGameManager::Render(){
	if (mPlaying){
		theCamera.SetPosition(mPlayer->GetPosition());
	} else {
		theCamera.SetPosition(Vector2(0, 0));
	}
	
}

void LudumGameManager::showStartScreen(){
	for (Actor* a : mStartScreenActors){
		theWorld.Add(a);
	}
}

void LudumGameManager::hideStartScreen(){
	for (Actor* a : mStartScreenActors){
		theWorld.Remove(a);
	}
}

void LudumGameManager::startGame(){
	if (mPlayer != nullptr){
		mPlayer->Destroy();
	}
	if (mMapManager != nullptr){
		delete mMapManager; //:TODO: destructor, currently mem leak and undestroyed platforms
	}
	
	mPlayer = new Player(Vector2(0, 55));
	theWorld.Add(mPlayer);

	mMapManager = new MapManager;

	mPlaying = true;
}

void LudumGameManager::stopGame(){
	theSound.PlaySound(res::snd::gameOver);
	printf("stopping game!\n");
	mPlayer->Destroy();
	delete mMapManager;
	mPlayer = nullptr;
	mMapManager = nullptr;

	mPlaying = false;
	showStartScreen();
}