#ifndef LUDUMGAMEMANAGER_HPP
#define LUDUMGAMEMANAGER_HPP

#include "stdafx.h"
#include "Player.hpp"
#include "MapManager.hpp"


class LudumGameManager : public GameManager{
public:
	LudumGameManager();

	virtual void Start();
	virtual void Stop();
	virtual void Update(float dt);
	virtual void Render();

	virtual void ReceiveMessage(Message *message) {}
private:
	//////////////////////////////////////////////////////////////////////////
	/// \brief Starts the game, call after game over or to start the first time
	//////////////////////////////////////////////////////////////////////////
	void startGame();

	void stopGame();

	void showStartScreen();

	void hideStartScreen();

	///< The current player actor
	Player* mPlayer;

	///< The MapGenerator currently in use
	MapManager* mMapManager;

	///< True if currently playing the game, else false
	bool mPlaying;

	int mPreviousScore;
	int mBestScore;


	std::vector<Actor*> mStartScreenActors;
};

#endif