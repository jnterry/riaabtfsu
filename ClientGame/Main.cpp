
#include "stdafx.h"
#include "LudumGameManager.hpp"
#include "Resources.hpp"
#include <time.h>
#include <stdlib.h>


int main(int argc, char* argv[]){

	res::loadResources();
	// get things going
	//  optional parameters:
	//		int windowWidth			default: 1024
	//		int windowHeight		default: 768
	//		std::string windowName	default: "Angel Engine"
	//		bool antiAliasing		default: false
	//		bool fullScreen			default: false
	//		bool resizable			default: false
	theWorld.Initialize(1024, 768, "Game Name", true, false, false);


	srand(time(NULL));

	theWorld.SetupPhysics(Vector2(0, -50));
	theWorld.SetGameManager(new LudumGameManager());

		
	// do all your setup first, because this function won't return until you're exiting
	theWorld.StartGame();
	
	// any cleanup can go here
	theWorld.Destroy();
	
	return 0;
}
