#include "stdafx.h"
#include "MapManager.hpp"
#include "Enemy.hpp"

MapManager::MapManager() 
	: mLastX(25), mLastY(50), mSpawnDist(100), mDespawnDist(50), mRndGen(std::random_device()())
	, mNormalDist(0, 1), mMinY(0){
	//make a starting platform:
	PhysicsActor* platform = new PhysicsActor;
	platform->SetSize(50, 300);
	platform->SetPosition(0, mMinY - 100);
	platform->SetColor(Color(0, 1, 0));
	platform->SetDensity(0);
	platform->InitPhysics();
	this->mActors.push_back(platform);
	theWorld.Add(platform);
}

MapManager::~MapManager(){
	for (Actor* a : mActors){
		a->Destroy();
	}
}

void MapManager::Update(Vector2 playerPos){
	//printf("player x: %f, mFarX: %f\n", playerPos.X, mFarX);
	while (mLastX - playerPos.X < mSpawnDist){
		spawnPlatform();
	}
	
	//despawn anything off screen
	if (mActors.size() > 0){
		Actor* plat = mActors.front();
		//printf("checking despawn, playerX: %f, platX: %f", playerPos.X, plat->GetPosition().X);
		while (mActors.size() > 0 && (playerPos.X - plat->GetBoundingBox().Max.X) > mDespawnDist){
			theWorld.Remove(plat);
			mActors.erase(mActors.begin());
			delete plat;
			plat = mActors.front();
			
		}
	}
}

PhysicsActor* MapManager::spawnPlatform(){
	PhysicsActor* platform = new PhysicsActor();
	platform->SetColor(Color(0, 1, 0));

	float w = genRndPlatformWidth();
	Vector2 delta = genRndGapDelta();
	Vector2 pos(mLastX + delta.X + w/2.0, mMinY-100);
	Vector2 size(w, ((mLastY + delta.Y) - pos.Y)*2.0);
	platform->SetSize(size);
	platform->SetPosition(pos);
	platform->SetDensity(0);
	platform->InitPhysics();
	mLastX = pos.X + size.X/2.0;
	mLastY = pos.Y + size.Y/2.0;
	mActors.push_back(platform);
	theWorld.Add(platform);

	float chance = (w / 40 + mNormalDist(mRndGen)) / 2;
	printf("enemy chance: %f", chance);
	if (chance > 0.3){
		Enemy* enemy = new Enemy(Vector2(mLastX - 10, mLastY));
		theWorld.Add(enemy);
		mActors.push_back(enemy);
	}
	return platform;
}

Vector2 MapManager::genRndGapDelta(){
	Vector2 r;
	do {
		r.X = mNormalDist(mRndGen)+5.0;
		r.Y = (mNormalDist(mRndGen)*2.0);
	} while (!(r.X > 2 && r.X < 10 && r.Y > -5 && r.Y < 3 && (mLastY+r.Y > mMinY) && r.Y*r.Y+r.X*r.X < 70));
	return r;
}

float MapManager::genRndPlatformWidth(){
	float r;
	do{
		r = 25 + mNormalDist(mRndGen) * 10;
	} while (!(r > 17 && r < 40));
	return r;
}