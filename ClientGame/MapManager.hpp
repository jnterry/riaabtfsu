#ifndef MAPMANAGER_HPP
#define MAPMANGER_HPP

#include "stdafx.h"
#include <vector>
#include <random>

///Managages the map, spawns and deletes actors to make it infinitly scrolling
class MapManager{
public:
	MapManager();

	~MapManager();

	///< updates the map, moving stuff, spawning new parts, etc
	void Update(Vector2 playerPos);
private:
	///< The platforms that can be walked on
	std::vector<Actor*> mActors;

	///< How far ahead things are spawn
	float mSpawnDist;

	///< How far away things are before being despawned
	float mDespawnDist;

	///< The far distance of x spawned in
	float mLastX;

	///< The Y of the most recently spawned platfom
	float mLastY;
	
	///< The minimum Y for a platform top
	float mMinY;

	std::mt19937 mRndGen;

	std::normal_distribution<float> mNormalDist;

	///< spans a new physics actor
	PhysicsActor* spawnPlatform();

	Vector2 genRndGapDelta();

	float genRndPlatformWidth();
};

#endif