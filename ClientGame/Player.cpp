#include "stdafx.h"
#include "Player.hpp"
#include "Bullet.hpp"
#include "Resources.hpp"
#include "WeaponLaser.hpp"

Player::Player(Vector2 pos) 
	: HealthyEntity(100,5,0), mRadius(1), mJumpForce(1000), mBuletSpawnOffset(Vector2(1.1,0)), mWeapon(new WeaponLaser(0.3)){
	this->SetDrawShape(ADS_Square);
	this->SetSize(Vector2(mRadius * 2, mRadius * 2));
	this->SetPosition(pos);
	this->SetColor(Color(0, 0, 1));
	this->SetFriction(0);
	this->SetRestitution(0);
	this->InitPhysics();
	this->GetBody()->SetFixedRotation(true);
	this->GetBody()->SetBullet(true);

	this->mWeapon->setFacingLeft(false);
	this->mWeapon->setTeam(0);
}

void Player::Update(float dt){
	HealthyEntity::Update(dt);
	mWeapon->Update(dt);

	auto pBody = this->GetBody();
	bool isTouchingGround = false;
	(pBody->GetContactList() != NULL);
	auto contact = pBody->GetContactList();
	while (contact != NULL){
		b2Vec2 loc = contact->contact->GetManifold()->localPoint;
		if (loc.x == 0){
			isTouchingGround = true;
			break;
		}
		contact = contact->next;
	}

	if ((theInput.IsKeyDown(GLFW_KEY_UP) || theInput.IsKeyDown(GLFW_KEY_SPACE)) && isTouchingGround){
		this->ApplyLinearImpulse(Vector2(0, mJumpForce*dt), Vector2(0, 0));
		theSound.PlaySound(res::snd::jump, 0.01);
	}
	if (theInput.IsKeyDown(GLFW_KEY_A)){
		mWeapon->fire(this->GetPosition() + mBuletSpawnOffset);
	}

	float curX = pBody->GetLinearVelocity().x;
	float  targetX = 25;
	if (curX < targetX){
		pBody->ApplyLinearImpulse(b2Vec2(60*dt, 0), pBody->GetLocalCenter());
	}

}