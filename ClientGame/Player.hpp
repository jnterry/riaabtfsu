#ifndef PLAYER_H
#define PLAYER_H

#include "stdafx.h"
#include "HealthyEntity.hpp"
#include "Weapon.hpp"

class Player : public HealthyEntity{
public:
	Player(Vector2 pos);

	void Update(float dt) override;

	
private:
	float mRadius;
	float mJumpForce;
	Weapon* mWeapon;
	Vector2 mBuletSpawnOffset;
};

#endif