#include "stdafx.h"
#include "Resources.hpp"

namespace res{
	namespace snd{
		AngelSampleHandle laser;
		AngelSampleHandle jump;
		AngelSampleHandle gameOver;
	}
	bool loadResources(){
		snd::laser = theSound.LoadSample("Resources/Sounds/laser.wav", false);
		snd::jump = theSound.LoadSample("Resources/Sounds/jump.wav", false);
		snd::gameOver = theSound.LoadSample("Resources/Sounds/game_over.wav", false);
		return true;
	}
}