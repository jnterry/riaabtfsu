#ifndef RESOURCES_HPP
#define RESOURCES_HPP

#include "stdafx.h"

namespace res{
	namespace snd{
		extern AngelSampleHandle laser;
		extern AngelSampleHandle jump;
		extern AngelSampleHandle gameOver;
	}

	bool loadResources();
}

#endif
