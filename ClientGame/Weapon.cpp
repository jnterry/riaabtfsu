#include "stdafx.h"
#include "Weapon.hpp"

Weapon::Weapon(float cooldownTime) : mCooldownTime(cooldownTime), mTimeSinceLastShot(cooldownTime){

}

bool Weapon::canFire(){
	return mCooldownTime < mTimeSinceLastShot;
}

void Weapon::fire(Vector2 spawnPos){
	if (!canFire()){ return; }
	onFire(spawnPos);
	mTimeSinceLastShot = 0;
}

void Weapon::Update(float dt){
	mTimeSinceLastShot += dt;
}

void Weapon::setFacingLeft(bool faceLeft){
	mFacingLeft = faceLeft;
}

void Weapon::setTeam(int team){
	mTeam = team;
}