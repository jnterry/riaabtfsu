#ifndef WEAPON_HPP
#define WEAPON_HPP

#include "stdafx.h"

class Weapon{
public:
	Weapon(float cooldownTime);
	bool canFire();
	void fire(Vector2 spawnPos);
	void Update(float dt);
	void setFacingLeft(bool faceLeft);
	void setTeam(int team);

	//////////////////////////////////////////////////////////////////////////
	/// \brief Virtual function to be overridden by derived classes, should spawn
	/// the bullet, add it the world, etc
	//////////////////////////////////////////////////////////////////////////
	virtual void onFire(Vector2 spawnPos) = 0;
protected:
	float mCooldownTime;
	float mTimeSinceLastShot;
	bool mFacingLeft;
	int mTeam;
};

#endif