#include "stdafx.h"
#include "WeaponLaser.hpp"
#include "Bullet.hpp"
#include "Resources.hpp"

WeaponLaser::WeaponLaser(float cooldown) : Weapon(cooldown){

}

void WeaponLaser::onFire(Vector2 spawnPos){
	Vector2 bulletVel;
	if (this->mFacingLeft){
		bulletVel = Vector2(-30, 0);
	} else {
		bulletVel = Vector2(50, 0);
	}
	theWorld.Add(new Bullet(3, spawnPos, bulletVel, mTeam));
	theSound.PlaySound(res::snd::laser, 0.5);
}