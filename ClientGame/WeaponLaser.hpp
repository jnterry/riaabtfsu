#ifndef WEAPON_LASER_HPP
#define WEAPON_LASER_HPP

#include "stdafx.h"
#include "Weapon.hpp"

class WeaponLaser : public Weapon{
public:
	WeaponLaser(float cooldown);
	void onFire(Vector2 spawnPos) override;
};

#endif